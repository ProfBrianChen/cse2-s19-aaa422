import java.util.Arrays;
import java.util.Random;
import java.lang.Math; 
import java.util.Scanner;
/*
Abdulaziz Alsalem
CSE2- HW09
4/16/2019

Objective:
 - generate() will produce integer arrays of a random size between 10 and 20 members.
 
 - print() will print out the members of an integer array input.
 
 - insert() will accept two arrays as inputs and produce a new array long enough to 
  contain both arrays, and both input arrays should be inserted into the new array.
  
 - shorten() should accept an input array and an input integer.  It should check if the 
  input integer actually refers to an index within the range of the input array’s length.
  If not, then return the original array.  Otherwise, return a new array having length 
  that is one member less than the input.  The output array in that case should have the 
  member referenced by the input index removed.
*/
public class ArrayGames{
    public static int [] generate(){//generate random arrays
        Random rand = new Random();
        int randomInt = rand.nextInt(11)+10;
        int [] array = new int[randomInt];
        for (int i = 0; i<array.length;i++){
            array[i] = rand.nextInt(20);
        }
        return array;
    }
    public static void print(int [] array){
        System.out.println(Arrays.toString(array));//print arrays
    }
    public static int [] insert(int [] array1, int [] array2){//insert the two arrays into one array
        Random rand = new Random();
        int [] newArray = new int[array1.length + array2.length];
        int randomInt = rand.nextInt(array1.length);
        if (randomInt<array1.length || randomInt<array2.length){
            for (int i = 0; i<randomInt;i++){
                newArray [i] = array1[i];
            }
            for (int j = 0;j<array2.length;j++){
                newArray[randomInt+j] = array2[j];
            }
            for (int k = randomInt;k<array1.length;k++){
                newArray[k+array2.length] = array1[k];
            }
            return newArray;
        } else {
            for (int l = 0; l < array1.length;l++){
                newArray[l] = array1[l];
            }
            for (int h = 0; h < array2.length;h++){
                newArray[h+array1.length]=array2[h];
            }
            return newArray;
        }
    }
    
    public static int [] shorten(int [] array, int val){//shorten the array by one index that's given
        int [] newArray = new int[array.length - 1];
        val++;
        if (val <= array.length){
            for (int i=0;i<val;i++){
                newArray[i] = array [i];
            }
            for (int j=val;j<array.length;j++){
                newArray[j-1] = array[j];
            }
            return newArray;
        } else {
            return array;
        }
    }
    
    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        int [] ar1;
        int [] ar2;
        System.out.print("Do you want to run insert or shorten? ");//ask user to choose
        String ans = scan.next();
        int val = 0;
        if (ans.equals("insert")){
             ar1 = generate();
             ar2 = generate();
             System.out.println("Input 1: ");
             print(ar1);
             System.out.println("Input 2: ");
             print(ar2);
             System.out.println("Output: ");
             print(insert(ar1,ar2));
        } else if (ans.equals("shorten")){
            ar1  = generate();
            System.out.println("Input 1: ");
            print(ar1);
            System.out.print("Input an integer in the index: ");
            val = scan.nextInt();
            System.out.println("Output: ");
            print(shorten(ar1,val));
        } else {
            System.out.println("Error: type either 'insert' or 'shorten'. ");//if user wrote something else, print error
        }
    }
}
