//Abdulaziz Alsalem
// CSE2 HW06 Network
// 19 March 2019
/*
Assignment. Write a program that displays a window 
into a “network” of boxes connected by lines.  
Query the user for the width and height of the display
window (in characters) and also the size of the boxes 
(they are squares of with identical height and width, 
in characters) and the length of an edge (lines between
boxes, in characters).  Draw the network of boxes 
starting from the upper left, starting always with a 
square.  Connect squares horizontally and vertically 
with an edge in the middle of the box.  Any inputs that
are not positive integers should be queried again until
the user provides a positive integer.

*/
import java.util.Scanner;

public class Network{

	public static void main(String[] args) {
	
   Scanner scan = new Scanner(System.in);

        
   boolean bool = true;
   int height = 0;
   while (bool)
   {
     System.out.print("Input height: ");
     bool = scan.hasNextInt();//check if int
            
     if (bool)
     {               
       height = scan.nextInt();                
       bool = false;
       if (height<=0)
       {   //check if pos
       bool = true;
       System.out.println("Error: type in a positive integer.");
           }
     } else
     {
       String junkWord = scan.next();//else remove value
       bool = true;
       System.out.println("Error: type in an integer.");
       }   
        }

    bool = true;
    int width = 0;
    while (bool)
    {
      System.out.print("Input width: ");
      bool = scan.hasNextInt();
      if (bool)
      {
        width = scan.nextInt();
        bool = false;
        if (width<=0)
        {
        bool = true;
        System.out.println("Error: type in a positive integer.");

            }
      } else 
      {
        String junkWord = scan.next();
        bool = true;
        System.out.println("Error: type in an integer.");
      }
        }
    
        bool = true;
        int square = 0;
        while (bool)
        {
            System.out.print("Input square size: ");
            bool = scan.hasNextInt();
            if (bool)
            {
                square = scan.nextInt();
              bool = false;
              if (square<=0)
              {
                bool = true;
                System.out.println("Error: type in a positive integer.");
            }
            } else 
            {
                String junkWord = scan.next();
                bool = true;
                System.out.println("Error: type in an integer.");
            }
            
        }

        bool = true;
        int length = 0;
        while (bool)
        {
            System.out.print("Input edge length: ");
            bool = scan.hasNextInt();
            if (bool)
            {
                length = scan.nextInt();
                bool = false;
              if (length<=0)
              {
                bool = true;
                System.out.println("Error: type in a positive integer.");
            }
            } else
            {
                String junkWord = scan.next();
                bool = true;
                System.out.println("Error: type in an integer.");
            }
            
        }
		String wide = "";
		
		String wide1 = "";
		
		String wide2 = "";
		
		String wide3 = "";
		
		String wode = "";
		
		String wode1 ="";
		
		String wode2 ="";
		
		String wode3 ="";
		
		for(int i = 0; i < width % (square + length);i++)
		{
			
			if(i == 0 || i == square - 1 && i <= square - 1)
			{
				wode += "#";
				wode1 += "|";
				wode2 += "|";
			}else if(i < square)
      {
				wode += "-";
				wode1 += " ";
				wode2 += " ";
			}else
      {
				wode += " ";
				wode1 += " ";
				wode2 += "-";
			}
			
			if(square % 2 == 0)
			{
				if(i == (square - 1) / 2 || i == (square - 1) / 2 + 1)
				{
					wode3 += "|";
				}else {
					wode3 += " ";
				}
			}else {
				if(i == (square - 1) / 2)
				{
					wode3 += "|";
				}else {
					wode3 += " ";
				}
			}
		}
		
		for(int i = 0; i < square + length; i++)
		{
			if(i == 0 || i == square - 1 && i <= square - 1)
			{
				wide += "#";
				wide1 += "|";
				wide2 += "|";
			}else if(i < square){
				wide += "-";
				wide1 += " ";
				wide2 += " ";
			}else {
				wide += " ";
				wide1 += " ";
				wide2 += "-";
			}
			
			if(square % 2 == 0)
			{
				if(i == (square - 1) / 2 || i == (square - 1) / 2 + 1)
				{
					wide3 += "|";
				}else {
					wide3 += " ";
				}
			}else {
				if(i == (square - 1) / 2)
				{
					wide3 += "|";
				}else {
					wide3 += " ";
				}
			}
			
			
		}
		
		int account = height / (square + length);
		
		for(int i = 0; i < height / (square + length); i++)
		{
			for(int k = 0; k < square + length; k++)
			{
				for(int j = 0; j < width / (square + length); j++)
				{	
					if(square % 2 == 0)
					{
						if(k == 0 || k == square - 1)
						{
							System.out.print(wide);
						}else if(k == square / 2 || k == square / 2 - 1)
						{
							System.out.print(wide2);
						}else if(k >= square){
							System.out.print(wide3);
						}else {
							System.out.print(wide1);
						}
					}else {
						if(k == 0 || k == square - 1)
						{
							System.out.print(wide);
						}else if(k == square / 2)
						{
							System.out.print(wide2);
						}else if(k >= square){
							System.out.print(wide3);
						}else {
							System.out.print(wide1);
						}
					}
					
				}
				
				int j = width % (square + length);
				if(square % 2 == 0)
				{
					if(k == 0 || k == square - 1)
					{
						System.out.print(wode);
					}else if(k == square / 2 || k == square / 2 - 1)
					{
						System.out.print(wode2);
					}else if(k >= square){
						System.out.print(wode3);
					}else {
						System.out.print(wode1);
					}
				}else {
					if(k == 0 || k == square - 1)
					{
						System.out.printf(wode);
					}else if(k == square / 2)
					{
						System.out.print(wode2);
					}else if(k >= square){
						System.out.print(wode3);
					}else {
						System.out.print(wode1);
					}
				}
				
				System.out.println();
			}
				
		}
		
		for(int k = 0; k < height % (square + length); k++)
		{
			for(int j = 0; j < width / (square + length); j++)
			{	
				if(square % 2 == 0)
				{
					if(k == 0 || k == square - 1)
					{
						System.out.print(wide);
					}else if(k == square / 2 || k == square / 2 - 1)
					{
						System.out.print(wide2);
					}else if(k >= square){
						System.out.print(wide3);
					}else {
						System.out.print(wide1);
					}
				}else {
					if(k == 0 || k == square - 1)
					{
						System.out.print(wide);
					}else if(k == square / 2)
					{
						System.out.print(wide2);
					}else if(k >= square){
						System.out.print(wide3);
					}else {
						System.out.print(wide1);
					}
				}
				
			}
			
			int j = width % (square + length);
			if(square % 2 == 0)
			{
				if(k == 0 || k == square - 1)
				{
					System.out.print(wode);
				}else if(k == square / 2 || k == square / 2 - 1)
				{
					System.out.print(wode2);
				}else if(k >= square){
					System.out.print(wode3);
				}else {
					System.out.print(wode1);
				}
			}else {
				if(k == 0 || k == square - 1)
				{
					System.out.printf(wode);
				}else if(k == square / 2)
				{
					System.out.print(wode2);
				}else if(k >= square){
					System.out.print(wode3);
				}else {
					System.out.print(wode1);
				}
			}
			
			System.out.println();
		}
		
		
	}
		
}