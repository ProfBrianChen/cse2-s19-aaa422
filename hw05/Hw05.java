//Abdulaziz Alsalem
// CSE 2 HW 5 loops
import java.util.Scanner;
/*
 ask for the course number, department name, the number of times it meets in a week, 
 the time the class starts, the instructor name, and the number of students.  
 For each item, check to make sure that what the user enters is actually of the correct 
 type, and if not, use an infinite loop to ask again. So if an integer needs to be entered, 
 make sure an integer is entered, and loop until one actually is. Use next() to remove 
 unwanted words when users do not type in a word of the correct type.  When a user types
 in an unwanted word (e.g. the wrong type) print an error message saying that you need 
 input of integer type or string type, etc.
*/
public class Hw05{
  public static void main(String[] args){
    
    Scanner scan=new Scanner(System.in);//declare scanner
    String junkWord = " ";
    
    boolean bool = true;
    int courseNum = 0;
    while (bool){//loop to check if input is correct
      System.out.print("What's the course number? (int) ");
      bool = scan.hasNextInt();//check if it's int
      if (bool){
        courseNum = scan.nextInt();//if true, use the value
        bool = false;
      } else {
        junkWord = scan.next(); // if false, don't use the value and repeat loop
        bool = true;
        System.out.println("Wrong Type, should be integer.");
      } 
      scan.nextLine();// to read the line after
    }
    bool = true;
    String deptName = " ";
    while (bool){
      System.out.print("What's the dept name? (str) "); 
      bool = scan.hasNext();//check if string
      if (bool){
        deptName = scan.nextLine();
        bool = false;
        if (deptName.contains("1") || deptName.contains("2") || deptName.contains("3") || deptName.contains("4")
           || deptName.contains("5") || deptName.contains("6") || deptName.contains("7") || deptName.contains("8")
           || deptName.contains("9") || deptName.contains("0")){
          bool = true;
          System.out.println("Wrong Type, should be string.");}
      } else {
        junkWord = scan.next(); 
        bool = true;
        System.out.println("Wrong Type, should be string.");
      } 
      }
    //scan.nextLine();
    bool = true;
    int meetingTimes = 0;
    while (bool){
      System.out.print("How many meetings per week? (int) ");
      bool = scan.hasNextInt();
      if (bool){
        meetingTimes = scan.nextInt();
        bool = false;
      } else {
        junkWord = scan.next(); 
        bool = true;
        System.out.println("Wrong Type, should be integer.");
      } 
    }
    scan.nextLine();
    bool = true;
    int classTime = 0;
    while (bool){
      System.out.print("What time does the class begin? (ex:1100) ");
      bool = scan.hasNextInt();
      //System.out.println(bool);
      if (bool){
        classTime = scan.nextInt();
        bool = false;
      } else {
        junkWord = scan.next(); 
        bool = true;
        System.out.println("Wrong Type, should be integer.");
      } 
    }
    scan.nextLine();
    bool = true;
    String instName = "";
    while (bool){
      System.out.print("What's the instructor's name? (str) ");
      bool = scan.hasNext();
      if (bool){
        instName = scan.nextLine();
        bool = false;
        if (instName.contains("1") || instName.contains("2") || instName.contains("3") || instName.contains("4")
           || instName.contains("5") || instName.contains("6") || instName.contains("7") || instName.contains("8")
           || instName.contains("9") || instName.contains("0")){
          bool = true;
          System.out.println("Wrong Type, should be string.");}
      } else {
        junkWord = scan.next(); 
        bool = true;
        System.out.println("Wrong Type, should be string.");
      } 
    }
    //scan.nextLine();
    bool = true;
    int studNum = 0;
    while (bool){
      System.out.print("How many students in the class? (int) ");
      bool = scan.hasNextInt();
      if (bool){
        studNum = scan.nextInt();
        bool = false;
      } else {
        junkWord = scan.next(); 
        bool = true;
        System.out.println("Wrong Type, should be integer.");
      } 
    }
    //scan.nextLine();
    
    
    
    System.out.println("Course number is "+courseNum + ", dept of " + deptName + ", "+meetingTimes+" meetings per week.");
    System.out.println("The class starts at "+classTime+", instructor's name is "+instName+", there are "+studNum+" students in the class.");
  }
}