// CSE02 
// hw03 Convert
// Abdulaziz Alsalem

// OBJECTIVE:
// The user wants to convert meters to inches, 
// which are stored in doubles.  Ask the user for a 
// measurement in meters, then print out the correct number of inches.  


import java.util.Scanner; //this command is for input through terminal
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); //scan command
          System.out.print("Enter the distance in meters: "); //print this in terminal 
          double meterValue = myScanner.nextDouble(); //get input  
          double inchValue = meterValue * 39.3701; // convert meters to inches
          String str = String.format("%3.4f", inchValue); // this command reduces the decimal digits to 4
          inchValue = Double.valueOf(str);
          System.out.print(meterValue + " meters is " + inchValue + " inches."); //print out the result
          System.out.println(" ");
        }  //end of main method   
} //end of class
