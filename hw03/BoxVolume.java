// CSE02 
// hw03 BoxVolume
// Abdulaziz Alsalem

// OBJECTIVE:
// Write a program that prompts the user for the 
// dimensions of a box: length, width and height. 
// The program should print out the volume inside the box.


import java.util.Scanner; //this command is for input through terminal
public class BoxVolume{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); // scan command
          System.out.print("The width side of the box is: "); // print this in terminal 
          double width = myScanner.nextDouble(); //get input  
          
          System.out.print("The length of the box is: "); // print this in terminal 
          double length = myScanner.nextDouble(); // get input  
          
          System.out.print("The height of the box is: "); // print this in terminal 
          double height = myScanner.nextDouble(); // get input  
          
          double volume = width * length * height; // calculate volume = sides ^3
          
          System.out.print("The volume inside the box is: " + volume); //print out the result
          System.out.println(" ");
          
        }  //end of main method   
} //end of class
