import java.util.Arrays;
import java.util.Random;
/*
Abdulaziz Alsalem
CSE2 - HW10
5/2/2019

Objective:
build the city, display it, invade the city, display it, 
then update and display in a loop 5 times.
*/
public class RobotCity{
    public static int[][] buildCity(){//generate 2D array of a city
        Random rand = new Random();
        int randomInt1 = rand.nextInt(6)+10;
        int randomInt2 = rand.nextInt(6)+10;
        int [][] cityArray = new int[randomInt1][randomInt2];
        int population = 0;
        for(int i= 0; i<cityArray.length;i++){
            for(int j = 0; j<cityArray[0].length;j++){
                population = rand.nextInt(900)+100;
                cityArray[i][j]=population; 
            }
            
        }
        return cityArray;
    }
    public static void display(int [][] cityArray){//display city
        for(int i= 0; i<cityArray.length;i++){
            for (int j = 0; j<cityArray[0].length;j++){
                System.out.printf("%4d",cityArray[i][j]);
            }
            System.out.printf("%n");
        }
    }
    public static void invade(int [][] cityArray, int k){//invade the city
        Random rand = new Random();
        int randomX = 0;
        int randomY = 0;
        for (int robot = 0; robot<=k;robot++){
            randomX = rand.nextInt(cityArray.length);
            randomY = rand.nextInt(cityArray[0].length);
            if (cityArray[randomX][randomY]<0){
                robot--;
                continue;
            } else{
                cityArray[randomX][randomY]=-cityArray[randomX][randomY];
            }
        }
    }
    public static void update(int [][] cityArray){//move robots in the 2D array to the right 
        int temp = 0;
        for (int i = 0; i<(cityArray.length) ; i++){
            for (int j = cityArray[0].length-1; j>=0;j--){
                if (cityArray[i][j] < 0){
                    cityArray[i][j] *= -1;
                    if (j == (cityArray[0].length-1)){
                        continue;
                    }
                    temp = cityArray[i][j+1];
                    cityArray[i][j+1] *=-1;// -cityArray[i][j+1]; 
                    }
                    
                }
            }
        }
    
    public static void main(String [] args){
        int [][] city;
         for (int i = 0; i<5;i++){
             city = buildCity();
             display(city);
             for(int j = 0; j<(city[0].length);j++){
                System.out.print("----"); 
             }
             System.out.println();
             System.out.println("                Robot Invasion"); 
             for(int j = 0; j<(city[0].length);j++){
                System.out.print("----"); 
             }
             System.out.println();
             invade(city,10);
             display(city);
             for(int j = 0; j<(city[0].length);j++){
                System.out.print("----"); 
             }
             System.out.println();
             System.out.println("                Robot Update"); 
             for(int j = 0; j<(city[0].length);j++){
                System.out.print("----"); 
             }
             System.out.println();
             update(city);
             display(city);
         }
    }
}
    