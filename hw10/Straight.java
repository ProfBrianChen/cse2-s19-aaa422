import java.util.Arrays;
import java.util.Random;
import java.lang.Math;
/*
Abdulaziz Alsalem
CSE2 - HW10
5/2/2019

Objective:
Calculate the probability of getting a straight using a 
random set of 5 cards that are shuffled from a deck. 
*/
public class Straight{
    public static int [] shuffle(){//generate 52 cards
        Random rand = new Random();
        int [] deck = new int[52];
        int randomInt = 0;
        int card = 0;
        for (card = 0; card<deck.length; card++){
            deck[card] = card;
        }
        int temp = 0;
        card = 0;
        for (card = 0; card<deck.length; card++){
            randomInt = rand.nextInt(52);
            temp = deck[randomInt];
            deck[randomInt] = deck[card];
            deck[card] = temp;
        }
        return deck;
    }
    public static int [] draw(int [] deck){
        int [] hand = new int [5];
        for (int card = 0;card<5;card++){
            hand[card] = deck[card];
        }
        return hand;
    }
    
    public static int[] rank(int [] hand){//rank the cards 
        int [] sortedHand = new int[hand.length];
        for (int i = 0;i<hand.length;i++){
            sortedHand[i] = hand[i];
        }
        int temp = 0;
        Arrays.sort(sortedHand);
        for(int i = 0;i<hand.length;i++){
            for (int j = i;j<hand.length; j++){
                if (sortedHand[j]%13 == 0 || sortedHand[j]%13 == 13 || sortedHand[j] == 0){
                    temp = sortedHand[i];
                    sortedHand[i] = sortedHand[j];
                    sortedHand[j] = temp;
                } else{
                    if (sortedHand[j]%13<sortedHand[i]%13){
                    temp = sortedHand[i];
                    sortedHand[i] = sortedHand[j];
                    sortedHand[j] = temp;
                    }
                }
            }
            
        }
        return sortedHand;
        /*
        hand[0] = first(sortedHand);
        hand[1] = second(sortedHand);
        hand[2] = third(sortedHand);
        hand[3] = fourth(sortedHand);
        hand[4] = fifth(sortedHand);
        Arrays.sort(sortedHand);
        System.out.print(Arrays.toString(sortedHand));
        */
    }
    
    public static int search(int[] hand, int k){//perform search to find the lowest
        switch (k){
            case 1:
                return first(hand);
            case 2:
                return second(hand);
            case 3:
                return third(hand);
            case 4:
                return fourth(hand);
            case 5:
                return fifth(hand);
            default:
                return 0;
        }
    }
    public static int first(int [] hand){
        int first;
        first = 99999;
        for (int i = 0; i<hand.length;i++){
            if (hand[i]<first){
                first = hand[i];
        }
        }
        return first;
    }
    
    public static int second(int [] hand){
        int first;
        int second;
        first = second = 99999;
        for (int i = 0; i<hand.length;i++){
            if (hand[i]<first){
                second = first;
                first = hand[i];
            } else if (hand[i]<second && hand[i] != first){
                second = hand[i];
            }
        }
        return second;
    }
    public static int third(int [] hand){
        int first;
        int second;
        int third;
        first = second = third = 99999;
        for (int i = 0; i<hand.length;i++){
            if (hand[i]<first){
                third = second;
                second = first;
                first = hand[i];
            } else if (hand[i]<second && hand[i] != first){
                third = second;
                second = hand[i];
            } else if (hand[i]<third && hand[i] != second){
                third = hand[i];
            }
            
        }
        return third;
    }
    public static int fourth(int [] hand){
        int first;
        int second;
        int third;
        int fourth;
        first = second = third = fourth = 99999;
        for (int i = 0; i<hand.length;i++){
            if (hand[i]<first){
                fourth = third;
                third = second;
                second = first;
                first = hand[i];
            } else if (hand[i]<second && hand[i] != first){
                fourth = third;
                third = second;
                second = hand[i];
            } else if (hand[i]<third && hand[i] != second){
                fourth = third;
                third = hand[i];
            } else if (hand[i]<fourth && hand[i] != third){
                fourth = hand[i];
            }
            
        }
        return fourth;
    }
    public static int fifth(int [] hand){
        int first;
        int second;
        int third;
        int fourth;
        int fifth;
        first = second = third = fourth = fifth = 9999;
        for (int i = 0; i<hand.length;i++){
            if (hand[i]<first){
                fifth = fourth;
                fourth = third;
                third = second;
                second = first;
                first = hand[i];
            } else if (hand[i]<second && hand[i] != first){
                fifth = fourth;
                fourth = third;
                third = second;
                second = hand[i];
            } else if (hand[i]<third && hand[i] != second){
                fifth = fourth;
                fourth = third;
                third = hand[i];
            } else if (hand[i]<fourth && hand[i] != third){
                fifth = fourth;
                fourth = hand[i];
            } else if (hand[i]< fifth && hand[i] != fourth){
                fifth = hand[i];
            }
        }
        return fifth;
    }
    public static boolean isStraight(int [] hand){//check if straight
        for(int i=0;i<(hand.length-1);i++){
            if (hand[i]%13 != (hand[i+1]%13-1)){
                return false;
            }
        }
        return true;
    }
    public static void main(String [] args){
        double count = 0;
        int hand[];
        for (int i = 0;i<(1000000);i++){
           hand = draw(shuffle()); 
           hand = rank(hand);
           if (isStraight(hand)){
               count++;
           }
        }
        
        double prop = 100.0*(count/1000000.0);
        System.out.printf("probability of straight is %5.4f",prop);
        
    }
}

