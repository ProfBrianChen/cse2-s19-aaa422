//Abdulaziz Alsalem
//CSE2 - Lab07
// 4/5/2019

// Objective:
// Generate artificial story using randomly generated sentences.

import java.util.Random;
import java.util.Scanner;

public class lab07{
  
  public static int random(){//generate random num
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    return randomInt;
}
  public static String adjectives(){
    int num = random();
    String adjective = "";
    switch (num){
      case 0:
        adjective = "grumpy";
        break;
      case 1:
        adjective = "beautiful";
        break;
      case 2:
        adjective = "handsome";
        break;
      case 3:
        adjective = "magnificent";
        break;
      case 4:
        adjective = "ugly";
        break;
      case 5:
        adjective = "gorgeous";
        break;
      case 6:
        adjective = "short";
        break;
      case 7:
        adjective = "angry";
        break;
      case 8:
        adjective = "tall";
        break;
      case 9:
        adjective = "lazy";
        break;
    }
    return adjective;
}
    public static String subjectNouns(){
    int num = random();
    String noun = "";
    switch (num){
      case 0:
        noun = "bird";
        break;
      case 1:
        noun = "fox";
        break;
      case 2:
        noun = "client";
        break;
      case 3:
        noun = "professor";
        break;
      case 4:
        noun = "dog";
        break;
      case 5:
        noun = "teacher";
        break;
      case 6:
        noun = "cat";
        break;
      case 7:
        noun = "president";
        break;
      case 8:
        noun = "panda";
        break;
      case 9:
        noun = "cookies";
        break;
    }
    return noun;
}
      public static String verbs(){
    int num = random();
    String verb = "";
    switch (num){
      case 0:
        verb = "dropped";
        break;
      case 1:
        verb = "played";
        break;
      case 2:
        verb = "read";
        break;
      case 3:
        verb = "passed";
        break;
      case 4:
        verb = "ran";
        break;
      case 5:
        verb = "caught";
        break;
      case 6:
        verb = "drove";
        break;
      case 7:
        verb = "cried";
        break;
      case 8:
        verb = "paid";
        break;
      case 9:
        verb = "taught";
        break;
    }
    return verb;
}
  public static String objectNouns(){
    int num = random();
    String nouns = "";
    switch (num){
      case 0:
        nouns = "ball";
        break;
      case 1:
        nouns = "warplanes";
        break;
      case 2:
        nouns = "bulldozer";
        break;
      case 3:
        nouns = "chemical plant";
        break;
      case 4:
        nouns = "ketchup";
        break;
      case 5:
        nouns = "donuts";
        break;
      case 6:
        nouns = "car";
        break;
      case 7:
        nouns = "aicraft";
        break;
      case 8:
        nouns = "soup";
        break;
      case 9:
        nouns = "helicopter";
        break;
    }
    return nouns;
}
  public static String sent1(){//thesis sentence
    String subject = subjectNouns();
    System.out.println("The "+adjectives()+" "+subject+" "+
                       verbs()+" the "+adjectives()+" "+objectNouns()+".");
    return subject;
  }
  public static void sent2(String subject){//body sentences
    System.out.println("This "+subject+" was also "+ adjectives()+" when the "+objectNouns()+" came.");
  }
  public static void sent3(String subject){
    System.out.println("Then, it "+ verbs() + " in front of a "+objectNouns()+".");
                       //subject+" was also "+ adjectives()+" when the "+objectNouns()+" came.");
  }
  public static void conclusion(String subject){//conclusion sentence
    System.out.println("At the end, that "+subject+" laughed at the "+ objectNouns()+".");
  }
  public static void paragraph(){//combine all the sentences
    int num = random();
    String subject = sent1();
    for (int i = 0;i<num;i++){
      if ((i%2)==0){
        sent2(subject);
      } else {
        sent3(subject);
      }
    }
    conclusion(subject);
  }
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    boolean bool = true;
    String string1 = "";
    
    while (bool){
      paragraph();
      System.out.println("Would you like another sentence? (type yes or no) ");
      string1 = scan.next();
      if (string1.equals("yes")){
        bool = true;
      } else if (string1.equals("no")){
        bool = false;
      } else {
        System.out.println("Please type yes or no.");
        bool = false;
      }
      
    }
  System.out.println("END");
  }
  
}