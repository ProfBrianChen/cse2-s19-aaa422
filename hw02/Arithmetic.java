// CSE02 
// hw02 Arithmetic Calculations
// Abdulaziz (Aziz) Alsalem 
// Tue, 5 Feb

public class Arithmetic{
  // OBJECTIVE:
  // This program computes & prints the cost of the -
  // items including the PA sales tax of 6%.
  public static void main(String args[]){
    System.out.println(" ");
    System.out.println("----------------");
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
      //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltPrice = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    
    //total cost of pants b4 tax
    double totalCostOfPants = numPants*pantsPrice;
    System.out.println("Pants cost $"+totalCostOfPants);
    //total cost of shirts b4 tax
    double totalCostOfShirts = numShirts*shirtPrice;
    System.out.println("Shirts cost $"+totalCostOfShirts);
    //total cost of belts b4 tax
    double totalCostOfBelts = numBelts*beltPrice;
    System.out.println("Belts cost $"+totalCostOfBelts);
    
    //total purchase b4 tax
    double totalCost = totalCostOfBelts+totalCostOfShirts+totalCostOfPants;
    System.out.println("Total before Tax is $"+totalCost);
    
    //sale tax on pants
    double taxOfPants = paSalesTax*totalCostOfPants;
    //sale tax on shirts
    double taxOfShirts = paSalesTax*totalCostOfShirts;
    //sale tax on belts
    double taxOfBelts = paSalesTax*totalCostOfBelts;
    
    //total sales tax
    double totalSaleTax = taxOfBelts+taxOfShirts+taxOfPants;
    String str = String.format("%1.2f", totalSaleTax);
    totalSaleTax = Double.valueOf(str);
    System.out.println("Sale Tax is $"+totalSaleTax);
    
    //total paid for this transaction, including tax
    double total = totalSaleTax+totalCost;
    //Delete last digits
    String str2 = String.format("%1.2f", total);
    total = Double.valueOf(str2);
    System.out.println("Total of purchase including PA tax is $"+total);
    System.out.println("----------------");
    System.out.println(" ");
  }
}