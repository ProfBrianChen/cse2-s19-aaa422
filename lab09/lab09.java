import java.util.Arrays;
import java.util.Random;
import java.lang.Math; 
import java.util.Scanner;
// Abdulaziz Alsalem
// CSE2 - lab09
/*
Objective:
Implement binary and linear searches on arrays.
*/
public class lab09{
    
    public static int [] randomArray(int size){
        Random rand = new Random();
        int[] array = new int[size];
        int randomInt;
        for (int i = 0; i<array.length;i++){
            randomInt = rand.nextInt(size);
            array[i] = randomInt;
        }
        return array;
            }
    public static void print(int [] array){
        System.out.println(Arrays.toString(array));
    }
    public static int [] randomAscendingArray(int size){
        Random rand = new Random();
        int[] array = new int[size];
        int randomInt1 = 0;
        int randomInt2 = 0;
        for (int i = 0; i<array.length;i++){
            if (i == 0){
                randomInt1 = rand.nextInt(10);
            } else {
                randomInt1 = rand.nextInt(5)+randomInt1;
                while (randomInt1 == randomInt2){
                    randomInt1 = rand.nextInt(5)+randomInt1;
                }
            }
        
            
            array[i] = randomInt1;
            randomInt2 = randomInt1;
        }
        return array;
    }
    public static int linearSearch(int [] array, int val){
        for (int i = 0; i<array.length;i++){
            if (array[i] == val){
                return i;
            }
        }
        return -1;
    }
    public static int binarySearch(int [] array, int val){
        int j = 0;
        int i = array.length-1;
        int mid;
        while(i >= j){
            mid = (i + j)/2;
            if (array[mid] == val){
                return mid;
            } else if (array[mid] > val){
                i = mid -1;
                
            } else if (array[mid] < val){
                j = mid +1;
            }
        }
        return -1;
    }
    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        int [] ar;
        System.out.print("Do you want to perform 'linear' or 'binary' search? ");
        String ans = scan.next();
        int val = 0;
        int size = 0;
        if (ans.equals("linear")){
             System.out.print("Input size of the array: ");
             size = scan.nextInt();
             ar = randomArray(size);
             System.out.println("Input: ");
             print(ar);
             System.out.print("Input an integer search term: ");
             val = scan.nextInt();
             System.out.println("Output: ");
             System.out.println(linearSearch(ar,val));
        } else if (ans.equals("binary")){
            System.out.print("Input size of the array: ");
            size = scan.nextInt();
            ar = randomAscendingArray(size);
            System.out.println("Input: ");
            print(ar);
            System.out.print("Input an integer search term: ");
            val = scan.nextInt();
            System.out.println("Output: ");
            System.out.println(binarySearch(ar,val));
        } else {
            System.out.println("Error: type either 'linear' or 'binary'. ");
        }
    }
}