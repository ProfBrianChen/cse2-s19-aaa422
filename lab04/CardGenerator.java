// CSE02 
// lab04 Card Generator
// Abdulaziz Alsalem

//OBJECTIVE:
// You are a magician and you need to practice your card
// tricks before your big show in Las Vegas. Since you 
// don’t want to reveal your secrets, you need to write
// a program that will pick a random card from the deck 
// so you can practice your tricks alone. Use a random 
// number generator to select a number
// from 1 to 52 (inclusive).  
// Each number represents one card, and the suits are 
// grouped:  Cards 1-13 represent the diamonds, 
// 14-26 represent the clubs, then hearts, then spades.
// In all suits, card identities ascend in step with
// the card number: 14 is the ace of clubs, 
// 15 is the 2 of clubs, and 26 is the king of clubs.

public class CardGenerator{
  public static void main(String args[]){
    int randomNumber = (int)(Math.random()*52 + 1);
    String suit;
    if (randomNumber <= 13){
      suit = "diamond";
    }
    else if (randomNumber <= 26){
      suit = "club";
    }
    else if (randomNumber <= 39){
      suit = "hearts";
    }
    else if (randomNumber <= 52){
      suit = "spades";
    }
    else{
      suit = "invalid";
    }
    
    int rem = randomNumber%13;
    
    String card = Integer.toString(rem);
    
    switch (rem){
      case 1:
        card = "Ace";
        break;
      case 11:
        card = "Jack";
        break;
      case 12:
        card = "Queen";
        break;
      case 0:
        card = "King";
        break;
      default:
        card = Integer.toString(rem);
        break;
    }
    
    System.out.println("You picked the " + card + " of " + suit);

  }
}
