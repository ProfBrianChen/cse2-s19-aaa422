// CSE02 
// lab02 Cyclometer
// Abdulaziz Alsalem

public class Cyclometer{
  
  public static void main(String args[]){
    //Objective:
    // Bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data,- 
    // the time elapsed in seconds, and the number of rotations of the front wheel during that time.- 
    // For two trips, given time and rotation count, program should:
    // print the number of minutes for each trip
    // print the number of counts for each trip
    // print the distance of each trip in miles
    // print the distance for the two trips combined

    System.out.println(" ");
    System.out.println("----------------");
    
    //Variables
    int secsTrip1=480;  //time for trip 1
    int secsTrip2=3220;  //time for trip 2
		int countsTrip1=1561;  //number of rotations for trip 1
		int countsTrip2=9037; //number of rotations for trip 2
    
    // Constants
    double wheelDiameter=27.0,  // diameter of the wheel of the bicycle
  	PI=3.14159, // pi 
  	feetPerMile=5280,  // conversion unit
  	inchesPerFoot=12,   // conversion unit
  	secondsPerMinute=60;  // conversion unit
    double distanceTrip1, distanceTrip2,totalDistance;  //
    
    // Print #1
    System.out.println("Trip 1 took "+
                       (secsTrip1/secondsPerMinute)+" minutes and had "+
                       countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
                       (secsTrip2/secondsPerMinute)+" minutes and had "+
                       countsTrip2+" counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI; // gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    // Print #2
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");


    System.out.println(" ");
    System.out.println("----------------");
  }
}