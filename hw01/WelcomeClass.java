// CSE02 
// hw01 Welcome Class
public class WelcomeClass{
  
  public static void main(String args[]){
    //print hello world to terminal window
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-a--a--a--4--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("v  v  v  v  v  v");   
    System.out.println("My name is Aziz Alsalem, I'm a first year student"); 
    System.out.println("majoring in either Materials or Chem Eng.");
    System.out.println("I like to code and learn about computers, that's why ");
    System.out.println("I plan to take a computer science minor."); 
  }
}