import java.util.Arrays;
/*
Abdulaziz Alsalem
CSE2 - HW10
5/2/2019

Objective:
For this lab, you will print out and operate on row- and column- major matrices
represented by two dimensional arrays.  Through the experience you will gain 
confidence with your abiliti es to operate on two dimensional arrays.

*/
public class lab10{
    public static int[][] increasingMatrix(int width, int height, boolean format){//output increasing matrix
        int [][] output = new int [width][height];//rows-columns
        int count = 1;
        if (format){//row-major
            for (int r= 0;r<width;r++){
                for (int c = 0; c<height;c++){
                    output[r][c] = count;
                    count++;
                }
            }
        } else {//column major
            for (int c= 0;c<height;c++){
                for (int r=0;r<width;r++){
                    output[r][c] = count;
                    count++;
                }
            }
        }
        return output;
    }
    public static void printMatrix( int[][] array, boolean format){
        System.out.println();
        System.out.print("[");
        if (format){//row-major
            for (int r = 0; r<array.length;r++){
                for (int c = 0; c<array[r].length;c++){
                    System.out.print(" " +array[r][c]+" ");
                }
                System.out.print("]");
                System.out.println();
                if (r == (array.length-1)){
                    
                } else {
                  System.out.print("[");  
                }
            }
            
        } else {//column-major
            //System.out.println();
            //System.out.print("[");
            for (int c = 0; c<array[0].length;c++){
                for (int r = 0; r<array.length;r++){
                    System.out.print(" "+array[r][c]+" ");
                }
                System.out.print("]");
                System.out.println();
                if (c == (array[0].length-1)){
                    
                }else{
                    System.out.print("[");
                }
            }
        }
    }
    public static int [][] translate(int[][] array){//column major --> row major
        int [][] output = new int [array.length][array[0].length];
        int x = 0;
        int [] stp1 = new int[array.length*array[0].length];
        for (int c= 0;c<array[0].length;c++){
            for (int r = 0;r<array.length;r++){
                stp1[x] = array[c][r];
                x++;
            }
        }
        //System.out.println(Arrays.toString(stp1));
        int i = 0;
        for (int r= 0;r<array.length;r++){
            for (int c = 0; c<array[0].length;c++){
                output[r][c] = stp1[i];
                i++;
            }
        }
        return output;
    }
    public static int [][] add(int[][] a, int[][]b){
        int [][] output = new int [a.length][b[0].length];
        for (int r = 0; r<a.length;r++){
                for (int c = 0; c<b[r].length;c++){
                    output[r][c] = a[r][c]+b[r][c];
                }
            }
            return output;
    }
    public static int [][] addMatrix( int[][] a, boolean formata, int[][] b, boolean formatb){//add matrices
        if (a.length == b.length && a[0].length == b[0].length){
            int [][] output = new int [a.length][b[0].length];
            if (formata == true && formatb == true){
                output = add(a,b);
            } else if (formata == false && formatb == false){
                a = translate(a);
                b = translate(b);
                output = add(a,b);
            } else if (formata == false && formatb == true){
                a = translate(a);
                output = add(a,b);
            } else if (formata == true && formatb == false){
                b = translate(b);
                output = add(a,b);
            } else{
                System.out.println("Error in boolean expressions!");
                return null;
            }
            return output;
            } else{
            System.out.println("arrays cannot be added!");
            return null;
        }
    }
    public static void main(String [] args){
        int [][] out = increasingMatrix(3,3,false);
        int [][] out2 = increasingMatrix(3,3,true);
        printMatrix(out, true);
        translate(out);
        printMatrix(out, false);
        //printMatrix(out, false);
        printMatrix(addMatrix(out,true,out2,true),true);
    }
}