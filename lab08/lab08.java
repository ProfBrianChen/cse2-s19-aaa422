/*
Abdulaziz Alsalem
CSE2 - lab08
Objective: work with arrays to output range, mean, standard deviation, and shuffled array.
*/

import java.util.Arrays;
import java.util.Random;
import java.lang.Math; 

public class lab08{
    
  public static int getRange(int [] array){//range method hieghest - lowest value
      Arrays.sort(array);
      int max = array[array.length-1];
      int min = array[0];
      int range = max-min;
      return range;
  }
  public static double getMean(int [] array){//mean
    double sum = 0.0;
    for (int i =0;i<array.length;i++){
        sum+=array[i];
    }
    double mean = sum/(array.length);
    return mean;
  }
  public static double getStdDev(int [] array){//standard deviation
    double sum = 0;
    double mean = getMean(array);
    for (int i =0;i<array.length;i++){
        sum+=Math.pow((array[i]-mean), 2);
    }
    double stdDev = Math.sqrt(sum/(array.length-1));
    return stdDev;
  }
  public static void shuffle(int [] array){
      Random rand = new Random();
      int randomInt = 0;
      for (int i = 0; i<array.length;i++){
          randomInt = rand.nextInt(array.length);
          /*
          while (randomInt < i){
              randomInt = rand.nextInt(array.length);
          }
          */
          array[i] = array[randomInt];
      }
      System.out.println(Arrays.toString(array));
  }
  public static void main(String[] args){
    Random rand = new Random();
    int randomInt = rand.nextInt((50) + 1) + 50;
    int[] array = new int[randomInt];
    System.out.println("Array Size: "+array.length);
    System.out.print("[");
    for (int i = 0; i<array.length;i++){
      randomInt = rand.nextInt(100);
      array[i] = randomInt;
      if (i == (array.length-1)){
          System.out.print(array[i]);
      } else{
          System.out.print(array[i]+", ");}
    }
    System.out.print("]");
    System.out.println();
    int range = getRange(array);//
    double mean = getMean(array);
    double stdDev = getStdDev(array);//
    System.out.println("Range: "+range + " Mean: "+mean+ " StdDv: "+stdDev);
    System.out.println("----------------------------");
    System.out.println("Shuffled Array: ");
    shuffle(array);
    }
}