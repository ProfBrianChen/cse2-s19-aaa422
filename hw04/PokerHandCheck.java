// CSE02 
// HW04 PokerHandCheck
// Abdulaziz Alsalem

// Objective:
// In a range of card games, a poker hand is defined as a 
// set of 5 playing cards randomly selected from one deck of cards.
// Several specific combinations of cards have special names, 
// such as “a pair”, “two pair”, or “three of a kind”.
// Some of these combinations are rarer than the others,
// but the probability of observing one such hand is very 
// different if you use more than one deck.  

public class PokerHandCheck{
  public static void main(String args[]){
    
    int randomNumber = (int)(Math.random()*52 + 1); // generate a random number
    String suit1; // declare suit variable
    if (randomNumber <= 13){ //Based on the random num, the suits are assigned.
      suit1 = "diamond";
    }
    else if (randomNumber <= 26){
      suit1 = "club";
    }
    else if (randomNumber <= 39){
      suit1 = "hearts";
    }
    else if (randomNumber <= 52){
      suit1 = "spades";
    }
    else{
      suit1 = "invalid";
    }
    
    int rem = randomNumber%13; //remainder of the division. 
    
    String card1 = Integer.toString(rem); // save rem value in the card variable
    
    switch (rem){ //switch statement to check the card if it's not a number
      case 1:
        card1 = "Ace";
        break;
      case 11:
        card1 = "Jack";
        break;
      case 12:
        card1 = "Queen";
        break;
      case 0:
        card1 = "King";
        break;
       default:
        card1= Integer.toString(rem);
    }
    
    System.out.println("the " + card1 + " of " + suit1); //print card and suit
    
    
    /////////////////////////////////////////////////
    
    
    randomNumber = (int)(Math.random()*52 + 1);
    String suit2;
    if (randomNumber <= 13){
      suit2 = "diamond";
    }
    else if (randomNumber <= 26){
      suit2 = "club";
    }
    else if (randomNumber <= 39){
      suit2 = "hearts";
    }
    else if (randomNumber <= 52){
      suit2 = "spades";
    }
    else{
      suit2 = "invalid";
    }
    
    rem = randomNumber%13;
    
    String card2 = Integer.toString(rem);
    
    switch (rem){
      case 1:
        card2 = "Ace";
        break;
      case 11:
        card2 = "Jack";
        break;
      case 12:
        card2 = "Queen";
        break;
      case 0:
        card2 = "King";
        break;
      default:
        card2 = Integer.toString(rem);
        break;
    }
    
    System.out.println("the " + card2 + " of " + suit2);
    
    
    /////////////////////////////////////////////////
    
    
    randomNumber = (int)(Math.random()*52 + 1);
    String suit3;
    if (randomNumber <= 13){
      suit3 = "diamond";
    }
    else if (randomNumber <= 26){
      suit3 = "club";
    }
    else if (randomNumber <= 39){
      suit3 = "hearts";
    }
    else if (randomNumber <= 52){
      suit3 = "spades";
    }
    else{
      suit3 = "invalid";
    }
    
    rem = randomNumber%13;
    
    String card3 = Integer.toString(rem);
    
    switch (rem){
      case 1:
        card3 = "Ace";
        break;
      case 11:
        card3 = "Jack";
        break;
      case 12:
        card3 = "Queen";
        break;
      case 0:
        card3 = "King";
        break;
      default:
        card3 = Integer.toString(rem);
        break;
    }
    
    System.out.println("the " + card3 + " of " + suit3);
    
    
    /////////////////////////////////////////////////
    
    
    randomNumber = (int)(Math.random()*52 + 1);
    String suit4;
    if (randomNumber <= 13){
      suit4 = "diamond";
    }
    else if (randomNumber <= 26){
      suit4 = "club";
    }
    else if (randomNumber <= 39){
      suit4 = "hearts";
    }
    else if (randomNumber <= 52){
      suit4 = "spades";
    }
    else{
      suit4 = "invalid";
    }
    
    rem = randomNumber%13;
    
    String card4 = Integer.toString(rem);
    
    switch (rem){
      case 1:
        card4 = "Ace";
        break;
      case 11:
        card4 = "Jack";
        break;
      case 12:
        card4 = "Queen";
        break;
      case 0:
        card4 = "King";
        break;
      default:
        card4 = Integer.toString(rem);
        break;
        
    }
    
    System.out.println("the " + card4 + " of " + suit4);
    
    
    /////////////////////////////////////////////////
    
    
    randomNumber = (int)(Math.random()*52 + 1);
    String suit5;
    if (randomNumber <= 13){
      suit5 = "diamond";
    }
    else if (randomNumber <= 26){
      suit5 = "club";
    }
    else if (randomNumber <= 39){
      suit5 = "hearts";
    }
    else if (randomNumber <= 52){
      suit5 = "spades";
    }
    else{
      suit5 = "invalid";
    }
    
    rem = randomNumber%13;
    
    String card5 = Integer.toString(rem);
    
    switch (rem){
      case 1:
        card5 = "Ace";
        break;
      case 11:
        card5 = "Jack";
        break;
      case 12:
        card5 = "Queen";
        break;
      case 0:
        card5 = "King";
        break;
      default:
        card5 = Integer.toString(rem);
        break;
    }
    
    System.out.println("the " + card5 + " of " + suit5);
    
    
    /////////////////////////////////////////////////
    String name = "high card hand";
    int count = 0;
    
    if (card1.equals(card2)){ // To check if values equal each other.
      count = count+1;
      
    }
    if (card1.equals(card3)){
      count = count+1;
      
    }
    if (card1.equals(card4)){
      count = count+1;
      
    }
    if (card1.equals(card5)){
      count = count+1;
      
    }
    if (card2.equals(card3)){
      count = count+1;
      
    }
    if (card2.equals(card4)){
      count = count+1;
      
    }
    if (card2.equals(card5)){
      count = count+1;
      
    }
    if (card3.equals(card4)){
      count = count+1;
      
    }
    if (card3.equals(card5)) {
      count = count+1;
    }
    if (card4.equals(card5)){
      count = count+1;
    }
    
    
    
    if (count == 1){ // special names are assigned based on how many values equal others.
      name = "pair";
    }
    if (count == 2){
      name = "two pair";
    }
    if (count >= 3){
      name = "three of a kind";
    }
    if (count == 0) {
      name = "high card hand";
    }
       
    System.out.println("You have a " + name + "!");
  }
}
