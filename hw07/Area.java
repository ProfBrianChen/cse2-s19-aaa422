/*
Abdulaziz Alsalem
3/26
CSE2 HW07

Write a program that can calculate the area of three different shapes;
a rectangle, a triangle and a circle. Let the user choose which shape 
they want by typing the words “rectangle”, “triangle” or “circle”.  
Then, have the user enter the values of the dimension(s) appropriate 
for each shape in the form of doubles.
*/
import java.util.Scanner;
import java.lang.Math.*;

public class Area{
  public static double input(String question){//method to accept values
    Scanner scan = new Scanner(System.in);       
    boolean bool = true;
    double val = 0.00;
    while (bool)
    {
      System.out.print(question);
      bool = scan.hasNextDouble();//check if double
            
      if (bool)
      {               
        val = scan.nextDouble();                
        bool = false;
        if (val<=0)
        {   //check if pos
          bool = true;
          System.out.println("Error: type in a positive double.");
        }
      } else
      {
        String junkWord = scan.next();//else remove value
        bool = true;
        System.out.println("Error: type in a double.");
      }   
    }
            
   
    return val;
  }
  public static double rectangle(){//rectangle method to calculate area
    double length = input("input length: ");
    double width = input("input width: ");
    double area = length*width;
    return area;
    
  }
  public static double triangle(){//triangle method to calculate area
    double height = input("input height: ");
    double base = input("input base: ");
    double area = 0.5*base*height;
    return area;
  }
  public static double circle(){//circle method to calculate area
    double radius = input("input radius: ");
    double area = Math.PI*2*radius;
    return area;
  }
  public static void main(String[] args){//main method
    Scanner scan = new Scanner(System.in);
    boolean bool = true;
    while (bool){
    System.out.print("Type in a shape: ");//ask use to input shape
    String shape = scan.next();//
    if (shape.equals("rectangle")){//check if rectangle
      System.out.println("Area of rectangle is "+rectangle());
      bool = false;
    }
    else if (shape.equals("triangle")){//check if triangle
      System.out.println("Area of triangle is "+triangle());
      bool = false;
    }
    else if (shape.equals("circle")){//check if circle
      System.out.println("Area of circle is "+circle());
      bool = false;
    }
    else{//if string is not identified
      //String junkWord = scan.next(); --- no need since the string has been accepted already.
      System.out.println("Error: shape not found");
      System.out.println("The only available shapes are rectangle, triangle, circle.");
      bool = true;
    }
  }
}
}
