/*
Abdulaziz Alsalem
3/26
CSE2 HW07

Write a program that can process a string by examining all the 
characters, or just a specified number of characters in the string, 
and determining if they are letters. Let the user enter a string and 
choose if he or she wants to examine all the characters or just a 
certain number.
*/
import java.util.Scanner;

public class StringAnalysis{
  public static boolean analyze(char chr ){//method to analyze string characters
    boolean bool = Character.isLetter(chr);
    return bool;
  }
  public static boolean analyze(int chr ){//method to accept integers and return false
    boolean bool= false;
    return bool;
  }
  public static void main(String[] args){//main method
    Scanner scan = new Scanner(System.in);//declare scanner
    System.out.print("Enter string: ");
    String string = scan.next();//accept string
    boolean state = true;
    while (state){
    System.out.print("Do you want to examine all characters? ");
    String yesno = scan.next();
    if (yesno.equals("yes")){//if yes, check every character
      boolean bool = false;
      boolean onlyLetters = true;
      state = false;
      for(int i = 0;i<(string.length());i++){//check every character
        bool = analyze(string.charAt(i));
        if (!(bool)){//if one of the characters is false, return false
          onlyLetters = false;
        }
      }
      //System.out.println(onlyLetters);
      if (onlyLetters){//print result
            System.out.println("There are no integers, only letters.");
          } else{
            System.out.println("There are integers.");
          }
    } else if (yesno.equals("no")){
      boolean state2 = true;
      while (state2){//check how many characters the user wants to check
        System.out.print("type in the number of characters you want to analyze: ");
        state2 = scan.hasNextInt();
        if (state2){
          int num = scan.nextInt();
          boolean bool = false;
          boolean onlyLetters = true;
          state2 = false;
          state = false;
          for(int i = 0;i<num;i++){
            bool = analyze(string.charAt(i));
            if (!(bool)){
              onlyLetters = false;
            }
          }
          if (onlyLetters){//print result
            System.out.println("There are no integers, only letters.");
          } else{
            System.out.println("There are integers.");
          }
          
        } else {
          String junkWord = scan.next();//else remove value
          state2 = true;
          System.out.println("Error: type in integers only.");
    
        }
      }
    } else {//if user typed something else other than yes or no
      String junkWord = scan.next();
      state = true;
      System.out.println("Error: type yes or no.");
    }
    }
  }
}