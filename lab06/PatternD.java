/*
Abdulaziz Alsalem
CSE 2
Lab06
March 21

Ask the user for an integer between 1 - 10. 
This will be the length of the pyramid 

Use nested loops that display the following pattern:

6 5 4 3 2 1
5 4 3 2 1
4 3 2 1
3 2 1
2 1
1


*/
import java.util.Scanner;
public class PatternD{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    
    boolean bool = true;
    int num = 0;
    while (bool){
    System.out.print("Input a positive integer b/w 1 and 10: ");
    bool = scan.hasNextInt();//check if int
            
     if (bool)
     {               
       num = scan.nextInt();                
       bool = false;
       if (num<=0 || num>10)
       {   //check if pos
       bool = true;
       System.out.println("Error: type in a positive integer b/w 1 and 10.");
       }
     } else
     {
       String junkWord = scan.next();//else remove value
       bool = true;
       System.out.println("Error: type in an integer.");
       }   
        }
    
    for (int i = 1;i<=num;i++){//determines the lines
      for (int j=(num+1)-i;j>=1;j--){//determines the columns
        System.out.print(j);
        System.out.print(" ");
      }
      System.out.println("");
    }
  }
}
