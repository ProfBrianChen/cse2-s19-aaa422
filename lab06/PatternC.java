/*
Abdulaziz Alsalem
CSE 2
Lab06
March 21

Ask the user for an integer between 1 - 10. 
This will be the length of the pyramid 

Use nested loops that display the following pattern:

       1   
      21
     321
    4321
   54321
  654321


*/
import java.util.Scanner;
public class PatternC{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    
    boolean bool = true;
    int num = 0;
    while (bool){
    System.out.print("Input a positive integer b/w 1 and 10: ");
    bool = scan.hasNextInt();//check if int
            
     if (bool)
     {               
       num = scan.nextInt();                
       bool = false;
       if (num<=0 || num>10)
       {   //check if pos
       bool = true;
       System.out.println("Error: type in a positive integer b/w 1 and 10.");
       }
     } else
     {
       String junkWord = scan.next();//else remove value
       bool = true;
       System.out.println("Error: type in an integer.");
       }   
        }
    
    for (int i = 1; i <= num; i++) {//determines the rows
      for (int j = num-i; j >= 1; j--) {//determines the space
        System.out.print(" ");
      }
      for (int k = i; k>=1;k--){//determines the columns
        System.out.print(k);
      }
      System.out.println("");
    }
  }
}
