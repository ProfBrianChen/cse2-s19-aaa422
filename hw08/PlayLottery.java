/*
Abdulaziz Alsalem
CSE2 - HW08
9/4

OBJECTIVE:
 Write a program to help a user play the lottery.  Your program 
 will take a series of 5 integers as input from the user and store 
 them in an array, generate a random series of 5 integers in the range 
 of 0 to 59, without replacement, and store them in another array, and then 
 compare the users numbers (in order) to the random numbers (in order).  
 If the user selected all 5 numbers correctly in the same order as the 
 computer they win, anything else and they lose.


*/
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class PlayLottery{
  public static boolean userWins(int[] user, int[] winning){//this method compares between the two array
    boolean bool = false;
    int count = 0;
    for (int i = 0;i<5;i++){
      if (user[i] == winning[i]){
        count++;}
    }
    
    if (count == 5){
      bool = true;
    } else {
      bool = false;
    }
    
    return bool;
  }
  //returns true when user and winning are the same.
  public static int[] numbersPicked(){
    Random randomGenerator = new Random();
    int randInt1 = randomGenerator.nextInt(60);
    int randInt2 = randomGenerator.nextInt(60);
    int randInt3 = randomGenerator.nextInt(60);
    int randInt4 = randomGenerator.nextInt(60);
    int randInt5 = randomGenerator.nextInt(60);
    
    int [] array = {randInt1,randInt2,randInt3,randInt4,randInt5};
    return array;
  }
//generates the random numbers for the lottery without 
//duplication.



  public static void main (String [] args){//main method
    Scanner scan = new Scanner(System.in);
    int [] array = numbersPicked();//fill array with random numbers
    System.out.println("Guess the five integers: ");
    int [] userArray = new int [5];
    for (int i = 0;i<5;i++){//ask user for integers to fill array
      System.out.print("Enter integer: ");
      userArray[i] = scan.nextInt();
    }
   
    boolean bool = userWins(userArray, array);//check if arrays match
    System.out.println();
    System.out.println("Your guess: "+Arrays.toString(userArray));
    System.out.println("Program's guess: "+Arrays.toString(array));
    System.out.println();
    if (bool){
      System.out.println("Congratulations! You Win. ");
    } else {
      System.out.println("You lose. ");
    }
    
  }
}