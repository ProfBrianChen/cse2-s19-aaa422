/*
Abdulaziz Alsalem
CSE2 - HW08
9/4

OBJECTIVE:
 Write a program that creates an array with an arbitrary size 
 and filled with random chars that are lowercase and uppercase letters. 
 Your program should then separate this first array into two different 
 arrays using two methods: getAtoM() and getNtoZ(). getAtoM() reads in the 
 arbitrary array as input and finds all the upper and lowercase letters 
 from A to M (inclusive), putting them into a new array that is returned as output.  
 getNtoZ() reads in the arbitrary array as input and finds all the upper and
 lowercase letters from N to Z (inclusive), putting them into a new array that
 is returned as output.


*/
import java.util.Random;
import java.util.Arrays;

public class Letters{
  public static void getAtoM(char [] array){//array to return A-M letters in array
    int count = 0;
    for (int i = 0; i<array.length;i++){
      if (array[i] <= 'm' && array[i] >= 'a' || array[i] <= 'M' && array[i] >= 'A'){ count++; }
    }
    char [] AtoM = new char [count];
    int j = 0;
    for (int i = 0; i<array.length;i++){
      
      if (array[i] <= 'm' && array[i] >= 'a' || array[i] <= 'M' && array[i] >= 'A'){
         
        AtoM[j] = array[i];
      } else{
        j--;
      }
      j++;
    }
    
    System.out.println("A to M characters: "+Arrays.toString(AtoM));
  }

  public static void getNtoZ(char [] array){//array to return N-Z letters in array
    int count = 0;
    for (int i = 0; i<array.length;i++){
      if (array[i] <= 'z' && array[i] >= 'n' || array[i] <= 'Z' && array[i] >= 'N'){ count++; }
    }
    char [] NtoZ = new char [count];
    int j = 0;
    for (int i = 0; i<array.length;i++){
      
      if (array[i] <= 'z' && array[i] >= 'n' || array[i] <= 'Z' && array[i] >= 'N'){
         
        NtoZ[j] = array[i];
      } else{
        j--;
      }
      j++;
    }
    
    System.out.println("N to Z characters: "+Arrays.toString(NtoZ));
  }

  public static void main(String [] args){
    Random randomGenerator = new Random();
    int randInt1 = randomGenerator.nextInt(10)+1;
    char [] array = new char [randInt1];
    
    char [] letters = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                       'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R', 'S', 'T', 'U', 'V', 'W', 'X','Y','Z'};
    //These letters are chosen randomly to fill the array
    int randInt2 = 0;
    for (int i = 0;i<randInt1;i++){//filling array with random letters
      randInt2 = randomGenerator.nextInt(52);
      array[i] = letters[randInt2];
    }
    
    System.out.println("Random character array: "+Arrays.toString(array));
    getAtoM(array);
    getNtoZ(array);
  }
}